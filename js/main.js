var button = document.querySelector("#next");
var cityInformation = [
     {
        title: "<h1>This is London</h1>",
        image: "<img src='img/london.jpeg'/>",
        text: "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>"
    },
    {
        title: "<h1>This is Paris</h1>",
        image: "<img src='img/paris.jpeg'/>",
        text: "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>"
    },
    {
        title: "<h1>This is Rome</h1>",
        image: "<img src='img/rome.jpeg'/>",
        text: "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>"
    },
    {
        title: "<h1>This is Bratislava</h1>",
        image: "<img src='https://www.visitbratislava.com/wp-content/uploads/2016/06/hrad_dunaj_ufo_lod.jpg'/>",
        text: "<p>Áfram Valur</p>"
    }
]
var counter = 0

var createStory = function () {
    return `
    ${cityInformation[counter].title}
    ${cityInformation[counter].image}
    ${cityInformation[counter].text}
    `;

}
button.onclick = function() {
    counter++;
    renderStory();
}

var renderStory = function() {
    document.querySelector("#story").innerHTML = createStory();
}
renderStory()

this===window?null:module.exports = {
    "createStory": createStory,
    "renderStory": renderStory
};
